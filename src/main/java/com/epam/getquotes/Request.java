package com.epam.getquotes;

import com.epam.validation.CustomException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * service class with default access modifier
 * his purpose to get html web site page
 */
public class Request
{
    private final String URL;

    /**
     * constructor
     * @param url - web site
     */
    public Request(String url)
    {
        URL = url;
    }

    /**
     * getting html page
     * @return html page in String format
     * @throws CustomException - when any thing happens
     */
    public String getData() throws CustomException {
        try {
            URL obj = new URL(URL);
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

            connection.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
        }
        catch (Exception e)
        {
            throw new CustomException("invalid url");
        }
    }

}
