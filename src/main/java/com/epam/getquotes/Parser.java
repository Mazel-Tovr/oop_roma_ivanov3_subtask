package com.epam.getquotes;

import com.epam.validation.CustomException;

import java.io.IOException;


/**
 *
 * his purpose to parse html response
 *
 */
public class Parser
{

    /**
     * String where we start parsing
     */
    private final String START_STRING;

    /**
     * Constructor
     * @param startString - start parsing string
     */
    public Parser(String startString) {
        START_STRING = startString;
    }

    /**
     * parse response String
     * @return Quote how was on site
     * @throws CustomException if site doesn't exists or
     */
    public String getQuote(String response) throws CustomException
    {
       int StartIndex = response.indexOf(START_STRING);
       int lastIndex = response.indexOf("\" />",StartIndex);
       if(StartIndex == -1 ) throw new CustomException("Такого сайта не существует");

       return response.substring(StartIndex + START_STRING.length(),lastIndex)
               .replaceAll("&#13;&#10;","\n")
               .replaceAll("&lt;","<").replaceAll("&gt;",">")
               .replaceAll("&quot;","\"");
    }

}
