package com.epam.validation;

public class InputDataControl {
//    private static InputDataControl instance = null;
//
//    private InputDataControl() {
//    }
//
//    public static InputDataControl getInstance() {
//        if (instance != null)
//            return instance;
//        synchronized (InputDataControl.class) {
//            if (instance == null) {
//                return new InputDataControl();
//            }
//        }
//        return instance;
//    }

    /**
     * checking is input is number
     *
     * @param value - input
     * @return true if number,false if not
     */
    public static boolean isNumber(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
